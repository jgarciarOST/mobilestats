package net.offshoretech.mobilestats;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = "TEST";
    private TelephonyManager tpm;
    private ConnectivityManager cm;
    TextView txt, txtClock;

    private SignalStrength      signalStrength;
    private final static String LTE_TAG             = "LTE_Tag";
    private final static String LTE_SIGNAL_STRENGTH = "getLteSignalStrength";

    public static final int UNKNOW_CODE = 99;
    int MAX_SIGNAL_DBM_VALUE = 31;

    TelephonyManager tel;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt = findViewById(R.id.txtTest);
        txtClock = findViewById(R.id.txtClock);
        Button btn = findViewById(R.id.btnConnect);

        //CLASE CON TODOS LOS VALORES
        final NetMonSignalStrenght netSignal = new NetMonSignalStrenght(getApplicationContext());
        //SERVICIOS
        cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        tpm = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        //------------------------------------------------------------------------------------------


        //DBM
        // Listener for the signal strength.
        final PhoneStateListener mListener = new PhoneStateListener()
        {
            @Override
            public void onSignalStrengthsChanged(SignalStrength sStrength)
            {
                signalStrength = sStrength;
                //getLTEsignalStrength();
                int asu = getSignalStrengthByName(sStrength, "getAsuLevel");
                Log.i(TAG, "ASU: " + asu);
                int db = (2*asu) - 113;
                Log.i(TAG, "DBM: " + db);

                //Clase NetMonSignal
                int netAsu = netSignal.getAsuLevel(signalStrength);
                Log.i(TAG, "ASUNET: " + netAsu);
                int netDbm = netSignal.getDbm(signalStrength);
                Log.i(TAG, "DBM;NET: " + netDbm);
                int netLevel = netSignal.getLevel(signalStrength);
                Log.i(TAG, "Level;NET: " + netLevel);
                int netLte = netSignal.getLteRsrq(signalStrength);
                Log.i(TAG, "Lte;NET: " + netLte);

                TextView txtLevel = findViewById(R.id.txtLevel);
                txtLevel.setText(
                        "NIVEL COBERTURA: " + "\n" +
                                "Asu: " + asu + "   Dbm: " + db + "\n" +
                                "AsuNet: " + netAsu + "   DbmANet: " + netDbm
                );

            }
        };
        // Register the listener for the telephony manager
        tpm.listen(mListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        //------------------------------------------------------------------------------------------


        //PERMISOS
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }
        // for example value of first element
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "NO PERMISISON", Toast.LENGTH_SHORT).show();
            return;
        }
        //------------------------------------------------------------------------------------------


        // CONECTIVIDAD
        Network[] networks = cm.getAllNetworks();

        String str = "";

        Log.i(TAG, "Network count: " + networks.length);
        for(int i = 0; i < networks.length; i++) {
            NetworkCapabilities caps = cm.getNetworkCapabilities(networks[i]);
            str = str + "   -   " + caps;
            Log.i(TAG, "Network " + i + ": " + networks[i].toString());
            Log.i(TAG, "Contents: " + caps.describeContents());
            //Log.i(TAG, "Contents: " + caps.);
            Log.i(TAG, "VPN transport is: " + caps.hasTransport(NetworkCapabilities.TRANSPORT_VPN));
            Log.i(TAG, "NOT_VPN capability is: " + caps.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_VPN));
        }
        //------------------------------------------------------------------------------------------


        //VPN

        NetworkInfo networkInfoVpn = cm.getNetworkInfo(ConnectivityManager.TYPE_VPN);
        Boolean isVpn = networkInfoVpn.isConnected();
        //String vpnInfo = networkInfoVpn.getSubtypeName();
        //int subtype = networkInfoVpn.getSubtype();
        //String vpnExtraInfo = networkInfoVpn.getExtraInfo();
        NetworkInfo.DetailedState ds = networkInfoVpn.getDetailedState();
        String detail = String.valueOf(ds);



        List<NetworkInterface> interfaces;
        NetworkInterface nVpn;
        String ip = "", hostName ="";
        int e = 0;
        try {
            interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            Log.i(TAG, "VPN SIZE: " + interfaces.size());
            for (NetworkInterface ni : interfaces) {
                // Pass over dormant interfaces
                Log.i(TAG, "INTERFACE NAME: " + ni.getName());
                if ("tun0".equals(ni.getName())){
                    // The VPN is up
                    List<InterfaceAddress> listInterVpn = ni.getInterfaceAddresses();
                    for(InterfaceAddress iaVpn : listInterVpn){
                        Log.i(TAG, "VPN IP is: " + iaVpn.getAddress().getHostAddress());
                        Log.i(TAG, "DISPOSITIVE NAME is: " + getHostName(ni.getName()));
                        //IP / Name
                        ip = iaVpn.getAddress().getHostAddress();
                        hostName = iaVpn.getAddress().getHostAddress();
                    }
                    nVpn = ni;
                }
                if (ni.isUp()){
                    Log.i(TAG, "Interface UP name is: " + ni.getName());
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
            Log.i(TAG, "ERROR VPN: " + ex.getMessage());
        }
        //------------------------------------------------------------------------------------------


        //TIEMPO Y HORA ACTUAL
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy  HH:mm:ss");
        String currentDateandTime = sdf.format(currentTime);

        txtClock.setText(currentDateandTime);
        //------------------------------------------------------------------------------------------


        //ESTADO DATOS
        NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
        String networkInfo = "";
        for(int z = 0; z < networks.length; z++) {

            networkInfo = networkInfo +  networkInfos[z].toString();
            Log.i(TAG, "NetworkInfo " +  ": " + networkInfos[z].toString());

        }
        //------------------------------------------------------------------------------------------


        //ALMACENAMIENTO
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = stat.getBlockSizeLong() * stat.getBlockCountLong();
        long megAvailable   = bytesAvailable / 1048576;
        Log.i(TAG,"Megs :"+megAvailable);

        //QUe tipo de almacenamiento
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        Log.i(TAG, "StorageExtAv: " + mExternalStorageAvailable + " || " + "StorageWrit: " + mExternalStorageWriteable);
        //------------------------------------------------------------------------------------------


        //NIVEL DE SO
        String myDeviceModel = android.os.Build.MODEL;
        int deviceApi = Build.VERSION.SDK_INT;
        String devRelease = Build.VERSION.RELEASE;
        Log.i(TAG, "Model: " + myDeviceModel + " - OS ANDROID: " + deviceApi + "TEST: " + devRelease);
        //------------------------------------------------------------------------------------------


        // WIFI, 4G TYPE OF CONNECTION
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chkConnectivity();
            }
        });
        //------------------------------------------------------------------------------------------


        //NIVEL COBERTURA
/*
        //Funciona en todos menos huawei
        TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        List<CellInfo> all = telephonyManager.getAllCellInfo();
        Log.i(TAG, "SIZE: " + all.size());
        CellInfoLte cellInfoLte = (CellInfoLte) all.get(0);
        CellSignalStrengthLte cellSignalStrengthLte = cellInfoLte.getCellSignalStrength();
        int strengthDbm = cellSignalStrengthLte.getDbm();
        Log.i("SIGNAL", String.valueOf(strengthDbm));
        txt.setText(strengthDbm + " dB");
*/
        //2a opción
        NetworkInfo activeNetInfo = cm
                .getActiveNetworkInfo();
        String wifinfo = "";

        String connect = "";
        if (activeNetInfo == null || !activeNetInfo.isConnectedOrConnecting()) {
            Log.i(TAG, "No connection");

        } else {
            int netType = activeNetInfo.getType();
            int netSubtype = activeNetInfo.getSubtype();

            if (netType == ConnectivityManager.TYPE_WIFI) {
                Log.i(TAG, "Wifi connection");
                connect = "WIFI";
                //GET WIFI INFO
                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                wifinfo = String.valueOf(wifiManager.getConnectionInfo());
                Log.i(TAG, "WIFI MANAGER:"  + wifiManager.getConnectionInfo());
                List<ScanResult> scanResult = wifiManager.getScanResults();
                Log.i(TAG, "SIZE WIFIS LIST: "+scanResult.size());
                /*
                for (int i = 0; i < scanResult.size(); i++) {
                    Log.i(TAG, "Speed of wifi"+scanResult.get(i).level);//The db level of signal
                    txt.setText("Speed of wifi"+scanResult.get(i).level);
                }
                */

            } else if (netType == ConnectivityManager.TYPE_MOBILE) {
                //txt.setText(activeNetInfo.getExtraInfo() +"//" + activeNetInfo.getDetailedState());
                Log.i(TAG, "GPRS/3G connection");
                connect = "LTE";
                // Need to get differentiate between 3G/GPRS
            }
        }
        //------------------------------------------------------------------------------------------


        //SET TEXT
        txt.setText("CONEXIÓN: " + connect + "\n" +
                "\n" + "WIFI INFO: " + wifinfo + "\n" +
                "\n" + "SISTEMA OPERATIVO: " + "  Api: " + deviceApi + "   Release: " + devRelease + "\n" +
                "\n" + "ALMACENAMIENTO: " + "\n" + "Megas disponibles:" + megAvailable + "\n" + "Lectura/Escritura:" + mExternalStorageAvailable +"/" + mExternalStorageWriteable  + "\n" +
                "\n" + "ESTADO CONEXIÓN: " + networkInfo + "\n" +
                "\n" + "VPN: " + isVpn + "  Detail: " +detail + " IP: " + ip + "  HostName: " + hostName
        );
        //------------------------------------------------------------------------------------------

    }



    private int getSignalStrengthByName(SignalStrength signalStrength, String methodName)
    {
        try
        {
            Class classFromName = Class.forName(SignalStrength.class.getName());
            java.lang.reflect.Method method = classFromName.getDeclaredMethod(methodName);
            Object object = method.invoke(signalStrength);
            return (int)object;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    private void getLTEsignalStrength()
    {
        try
        {
            Method[] methods = android.telephony.SignalStrength.class.getMethods();

            for (Method mthd : methods)
            {
                if (mthd.getName().equals(LTE_SIGNAL_STRENGTH))
                {
                    int LTEsignalStrength = (Integer) mthd.invoke(signalStrength, new Object[] {});
                    Log.i(TAG, "signalStrengthLTE = " + LTEsignalStrength);
                    return;
                }
            }
        }
        catch (Exception e)
        {
            Log.e(LTE_TAG, "Exception: " + e.toString());
        }
    }

    private  boolean chkConnectivity() {
        boolean isConnected = false;

        ConnectivityManager connManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo mobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo mVpn = connManager.getNetworkInfo(ConnectivityManager.TYPE_VPN);
        if( mobile.isAvailable() && mobile.getDetailedState() == NetworkInfo.DetailedState.CONNECTED ){
            Toast.makeText(getApplicationContext(), "4g/3G LTE", Toast.LENGTH_SHORT).show();
            return true;
        }
        if (mWifi.isAvailable() && !mVpn.isAvailable() && mWifi.getDetailedState() == NetworkInfo.DetailedState.CONNECTED ) {
            Toast.makeText(getApplicationContext(), "WiFi", Toast.LENGTH_SHORT).show();
            return true;
        }
        if (mVpn.isAvailable() && mWifi.isAvailable() && mVpn.getDetailedState() == NetworkInfo.DetailedState.CONNECTED ) {
            Toast.makeText(getApplicationContext(), "WIFI + VPN", Toast.LENGTH_SHORT).show();
            return true;
        }
        try {
            TelephonyManager mgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = mgr.getNetworkType();
            if (networkType == TelephonyManager.NETWORK_TYPE_GPRS){
                Class cmClass = Class.forName(connManager.getClass().getName());
                Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
                method.setAccessible(true);
                isConnected = (Boolean)method.invoke(connManager);
                if (isConnected){
                    Toast.makeText(getApplicationContext(), "GPRS", Toast.LENGTH_SHORT).show();
                    return isConnected;
                }
            }
            Toast.makeText(getApplicationContext(), "No connection", Toast.LENGTH_SHORT).show();
            return isConnected;

        } catch (Exception e) {
            return false;
        }

    }

    private static String getConnectionType(int type, int subType) {
        if (type == ConnectivityManager.TYPE_WIFI) {
            return "TYPE_WIFI";
        } else if (type == ConnectivityManager.TYPE_MOBILE) {
            switch (subType) {
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                    return "TYPE_UNKNOWN";
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return "TYPE_1XRTT"; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return "TYPE_CDMA"; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return "TYPE_EDGE"; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return "TYPE_EVDO_0"; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return "TYPE_EVDO_A"; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return "TYPE_GPRS"; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return "TYPE_HSDPA"; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return "TYPE_HSPA"; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return "TYPE_HSUPA"; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return "TYPE_UMTS"; // ~ 400-7000 kbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return "TYPE_IDEN"; // ~25 kbps


                // Unknown
                default:
                    return "TYPE_UNKNOWN";
            }
        } else {
            return "TYPE UNKNOWN";
        }
    }

    public static String getHostName(String defValue) {
        try {
            Method getString = Build.class.getDeclaredMethod("getString", String.class);
            getString.setAccessible(true);
            return getString.invoke(null, "net.hostname").toString();
        } catch (Exception ex) {
            return defValue;
        }
    }

}




